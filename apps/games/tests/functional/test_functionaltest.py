from django.test import TestCase
from apps.games.models import Game

class GameModelFunctionalTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super(GameModelFunctionalTest, cls).setUpClass()
        cls.test_game1 = Game(last_move_player1="Paper",last_move_player2="Rock")
        cls.test_game1.save()
        cls.test_game2 = Game(last_move_player1="Paper",last_move_player2="Scissors")
        cls.test_game2.save()
        cls.test_game3 = Game(last_move_player1="Rock",last_move_player2="Scissors")
        cls.test_game3.save()
        cls.test_game4 = Game(last_move_player1="Rock",last_move_player2="Paper")
        cls.test_game4.save()
        cls.test_game5 = Game(last_move_player1="Scissors",last_move_player2="Paper")
        cls.test_game5.save()
        cls.test_game6 = Game(last_move_player1="Scissors",last_move_player2="Rock")
        cls.test_game6.save()
        cls.test_game7 = Game(last_move_player1="Paper",last_move_player2="Rock")
        cls.test_game7.save()
        cls.test_game8 = Game(last_move_player1="Paper",last_move_player2="Scissors")
        cls.test_game8.save()
        cls.test_game9 = Game(last_move_player1="Rock",last_move_player2="Scissors")
        cls.test_game9.save()
        cls.test_game10 = Game(last_move_player1="Rock",last_move_player2="Paper")
        cls.test_game10.save()
        cls.test_game11 = Game(last_move_player1="Scissors",last_move_player2="Paper")
        cls.test_game11.save()
        cls.test_game12 = Game(last_move_player1="Scissors",last_move_player2="Rock")
        cls.test_game12.save()

    def test_moves_game(self):
        self.assertEquals(1, self.test_game1.score())
        self.assertEquals(2, self.test_game2.score())
        self.assertEquals(1, self.test_game3.score())
        self.assertEquals(2, self.test_game4.score())
        self.assertEquals(1, self.test_game5.score())
        self.assertEquals(2, self.test_game6.score())
        self.assertEquals(1, self.test_game7.score())
        self.assertEquals(2, self.test_game8.score())
        self.assertEquals(1, self.test_game9.score())
        self.assertEquals(2, self.test_game10.score())
        self.assertEquals(1, self.test_game11.score())
        self.assertEquals(2, self.test_game12.score())
