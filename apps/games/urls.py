from django.conf.urls import url

from .views import *
urlpatterns = [
    url(r'^$', Begin.as_view(), name='begin'),
    url(r'^player1/(?P<pk>\d+)$', Player1.as_view(), name='player1'),
    url(r'^player2/(?P<pk>\d+)$', Player2.as_view(), name='player2'),
    url(r'^win/(?P<pk>\d+)$', Win.as_view(), name='win'),
    url(r'^historical$', Historical.as_view(), name='historical'),
    url(r'^statistics$', Statistics.as_view(), name='statistics'),
]
