# -*- coding: utf-8 -*-
from django import forms
from .models import *


class BeginForm(forms.ModelForm):
    class Meta:
        model = Game
        fields = ("player1", "player2" )
    def clean(self):
        form_data = self.cleaned_data
        return form_data

class Player1Form(forms.ModelForm):
    class Meta:
        model = Game
        fields = ("last_move_player1",)
    def clean(self):
        form_data = self.cleaned_data
        return form_data

class Player2Form(forms.ModelForm):
    class Meta:
        model = Game
        fields = ("last_move_player2",)
    def clean(self):
        form_data = self.cleaned_data
        return form_data

