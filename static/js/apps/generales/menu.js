var mouseAdentro = false;//Variable para que no se ejecute dos veces seguidas las funciones mouse over o moseout

var handleSidebarMinifyMouseHover = function () {
    var funcion = function (e) {
        e.preventDefault();
        var sidebarClass = 'page-sidebar-minified';
        var targetContainer = '#page-container';
        $('#sidebar [data-scrollbar="true"]').css('margin-top', '0');
        $('#sidebar [data-scrollbar="true"]').removeAttr('data-init');
        $('#sidebar [data-scrollbar=true]').stop();
        if ($(targetContainer).hasClass(sidebarClass)) {
            $(targetContainer).removeClass(sidebarClass);
            if ($(targetContainer).hasClass('page-sidebar-fixed')) {
                if ($('#sidebar .slimScrollDiv').length !== 0) {
                    $('#sidebar [data-scrollbar="true"]').slimScroll({destroy: true});
                    $('#sidebar [data-scrollbar="true"]').removeAttr('style');
                }
                generateSlimScroll($('#sidebar [data-scrollbar="true"]'));
                //$('#sidebar [data-scrollbar=true]').trigger('mouseover');
            } else if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                if ($('#sidebar .slimScrollDiv').length !== 0) {
                    $('#sidebar [data-scrollbar="true"]').slimScroll({destroy: true});
                    $('#sidebar [data-scrollbar="true"]').removeAttr('style');
                }
                generateSlimScroll($('#sidebar [data-scrollbar="true"]'));
            }
        } else {
            $(targetContainer).addClass(sidebarClass);

            if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                if ($(targetContainer).hasClass('page-sidebar-fixed')) {
                    $('#sidebar [data-scrollbar="true"]').slimScroll({destroy: true});
                    $('#sidebar [data-scrollbar="true"]').removeAttr('style');
                }
                //$('#sidebar [data-scrollbar=true]').trigger('mouseover');
            } else {
                $('#sidebar [data-scrollbar="true"]').css('margin-top', '0');
                $('#sidebar [data-scrollbar="true"]').css('overflow', 'visible');
            }
        }
        //$(window).trigger('resize');
    };
    var viewportWidth = $(window).width();

    $('#sidebar').mouseover(function (e) {
        if (!mouseAdentro && (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) && viewportWidth > 600){
            funcion(e);
            mouseAdentro  = true;
        }

    });
    // $('#sidebar').mouseout(function (e) {
    //     if (mouseAdentro && (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) && viewportWidth > 600){
    //         //if(e.target === e.currentTarget) funcion(e);
    //         //mouseAdentro  = false;
    //     }
    // });

    $("#content, #header").mouseover(function(e) {
        if (mouseAdentro && (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) && viewportWidth > 600){
            funcion(e);
            mouseAdentro  = false;
        }
    });


    if (viewportWidth < 600) {
         $("#page-container").removeClass("page-sidebar-minified");
         $(window).trigger('resize');
    }


    $(window).on('load, resize', function mobileViewUpdate() {
        viewportWidth = $(window).width();
        if (viewportWidth < 600) {
             $("#page-container").removeClass("page-sidebar-minified");
        }else {
            $("#page-container").addClass("page-sidebar-minified");
        }
    });
};

handleSidebarMinifyMouseHover();